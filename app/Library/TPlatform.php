<?php
/**
 * Created by PhpStorm.
 * User     : mehmethakkioglu
 * Developer: Mehmet HAKKIOĞLU
 * Mail     : mehmethakkioglu@yandex.com
 * Date     : 15.01.2022
 * Time     : 12:42
 */

namespace App\Library;

use Illuminate\Http\Request;

trait TPlatform
{
    private $platform;
    private $config;

    public function __construct(Request $request)
    {
        $this->platform = $request->segment(4);
        $this->config = [
            'platform' => $this->platform,
            'token' => 'as89dua8s9duajsdasdhasdkasd'
        ];
    }
}
