<?php
/**
 * Created by PhpStorm.
 * User     : mehmethakkioglu
 * Developer: Mehmet HAKKIOĞLU
 * Mail     : mehmethakkioglu@yandex.com
 * Date     : 15.01.2022
 * Time     : 01:35
 */

namespace App\Library;

use Illuminate\Http\Request;

interface IPlatform
{
    /**
     * API aracılığı ile platforma bağlanmmak için kullandığımız fonksiyonumuz.
     *
     * @param array $config
     * @return mixed
     */
    public function connect(Request $request);

    /**
     * Giriş işlemi sonrası callback almak (kullanıcı bilgileri, yetkileri vb.) ve bu bilgileri veritabanına kayıt
     * için kullandığımız fonksiyonumuz.
     *
     * @return mixed
     */
    public function callback(Request $request);

    /**
     * Paylaşımların sistemimize kaydedilmesi için kullandığımız fonksiyonumuz. Planlanacak olan gönderi öncelikle
     * bu fonksiyon aracılığı ile önce sisteme kaydedilecek, ardından publish fonksiyonu ile auto-publish edilecek.
     *
     * @param array $config
     * @return mixed
     */
    public function share(Request $request);

    /**
     * Planlanan gönderilerimizin veritabanından kontrolü yapıldıktan sonra paylaşımının yapılacağı fonksiyondur.
     * share fonksiyonundan sonra CRONJOB zamanı gelince planlanan gönderinin yayını için bu fonksiyonu ziyaret ediyor.
     *
     * @param array $config
     * @return mixed
     */
    public function publish(Request $request, int $id);
}
