<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountsToUser extends Model
{
    use HasFactory;

    protected $table = 'account_to_user';

    protected $fillable = ['user_id','store_id','platform_id','account_id','status'];

    public $timestamps = false;
}
