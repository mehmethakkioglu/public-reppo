<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermissionToModuleRoute extends Model
{
    use HasFactory;

    protected $table = 'permission_to_module_route';

    protected $fillable = ['permission_id','module_route_id'];

    public $timestamps = false;
}
