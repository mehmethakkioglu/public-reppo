<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreToPlatform extends Model
{
    use HasFactory;

    protected $table = 'store_to_platform';

    protected $fillable = ['store_id','platform_id'];

    public $timestamps = false;
}
