<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageInfo extends Model
{
    use HasFactory;

    protected $table = 'package_info';

    protected $fillable = ['package_id','price','description'];

    public $timestamps = false;
}
