<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusGroup extends Model
{
    use HasFactory;

    protected $table = 'status_group';

    protected $fillable = ['title','options'];
}
