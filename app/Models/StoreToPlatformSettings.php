<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreToPlatformSettings extends Model
{
    use HasFactory;

    protected $table = 'platform_settings';

    protected $fillable = ['store_id','platform_id','size','other'];

    public $timestamps = false;
}
