<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserToPermission extends Model
{
    use HasFactory;

    protected $table = 'user_to_permission';

    protected $fillable = ['user_id','permission_id'];

    public $timestamps = false;
}
