<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserToStore extends Model
{
    use HasFactory;

    protected $table = 'user_to_store';

    protected $fillable = ['user_id','store_id'];

    public $timestamps = false;
}
