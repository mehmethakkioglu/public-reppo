<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageProperties extends Model
{
    use HasFactory;

    protected $table = 'package_properties';

    protected $fillable = ['package_id','name','value'];

    public $timestamps = false;
}
