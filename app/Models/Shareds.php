<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shareds extends Model
{
    use HasFactory;

    protected $table = 'shareds';

    protected $fillable = ['code','user_id','account_id','content','status_id','publish_date'];
}
