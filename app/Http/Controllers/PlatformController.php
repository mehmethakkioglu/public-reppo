<?php
/**
 * Created by PhpStorm.
 * User     : mehmethakkioglu
 * Developer: Mehmet HAKKIOĞLU
 * Mail     : mehmethakkioglu@yandex.com
 * Date     : 28.12.2021
 * Time     : 01:48
 */

namespace App\Http\Controllers;

use App\Library\TPlatform;
use App\Library\IPlatform;
use Illuminate\Http\Request;

class PlatformController extends Controller implements IPlatform
{
    use TPlatform;

    /**
     * API aracılığı ile platforma bağlanmmak için kullandığımız fonksiyonumuz.
     *
     * @param array $config
     * @return mixed
     */
    public function connect(Request $request)
    {
        return $this->config;
    }

    /**
     * Giriş işlemi sonrası callback almak (kullanıcı bilgileri, yetkileri vb.) ve bu bilgileri veritabanına kayıt
     * için kullandığımız fonksiyonumuz.
     *
     * @return mixed
     */
    public function callback(Request $request)
    {
        

    }

    /**
     * Paylaşımların sistemimize kaydedilmesi için kullandığımız fonksiyonumuz. Planlanacak olan gönderi öncelikle
     * bu fonksiyon aracılığı ile önce sisteme kaydedilecek, ardından publish fonksiyonu ile auto-publish edilecek.
     *
     * @param array $config
     * @return mixed
     */
    public function share(Request $request)
    {
        // TODO: Implement share() method.
    }

    /**
     * Planlanan gönderilerimizin veritabanından kontrolü yapıldıktan sonra paylaşımının yapılacağı fonksiyondur.
     * share fonksiyonundan sonra CRONJOB zamanı gelince planlanan gönderinin yayını için bu fonksiyonu ziyaret ediyor.
     *
     * @param array $config
     * @return mixed
     */
    public function publish(Request $request, int $id)
    {
        // TODO: Implement publish() method.
    }
}
