<?php
/**
 * Created by PhpStorm.
 * User     : mehmethakkioglu
 * Developer: Mehmet HAKKIOĞLU
 * Mail     : mehmethakkioglu@yandex.com
 * Date     : 28.12.2021
 * Time     : 01:48
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SettingsController extends Controller
{
    public function settings(Request $request)
    {
        return response()->json([
            'code' => 200,
            'message' => 'Ayalar'
        ],200);
    }

    public function optimize(Request $request)
    {
        Artisan::call('optimize');
        print_r(Artisan::output());die;
    }

    public function migrate(Request $request)
    {
        return ['Güncel'];
        Artisan::call('migrate');
        Artisan::call(sprintf('db:seed --class=%s','Users'));
        Artisan::call(sprintf('db:seed --class=%s','Store'));
        Artisan::call(sprintf('db:seed --class=%s','Platforms'));
        Artisan::call(sprintf('db:seed --class=%s','Accounts'));
        Artisan::call(sprintf('db:seed --class=%s','StatusGroup'));
        Artisan::call(sprintf('db:seed --class=%s','Modules'));
        Artisan::call(sprintf('db:seed --class=%s','Permissions'));
        Artisan::call(sprintf('db:seed --class=%s','ModuleRoutes'));
        Artisan::call(sprintf('db:seed --class=%s','UserToStore'));
        Artisan::call(sprintf('db:seed --class=%s','AccountsToUser'));
        Artisan::call(sprintf('db:seed --class=%s','StoreToPlatform'));
        Artisan::call(sprintf('db:seed --class=%s','StoreToPlatformSettings'));
        Artisan::call(sprintf('db:seed --class=%s','Status'));
    }
}
