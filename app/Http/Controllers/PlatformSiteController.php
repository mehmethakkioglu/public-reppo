<?php
/**
 * Created by PhpStorm.
 * User     : mehmethakkioglu
 * Developer: Mehmet HAKKIOĞLU
 * Mail     : mehmethakkioglu@yandex.com
 * Date     : 28.12.2021
 * Time     : 01:48
 */

namespace App\Http\Controllers;

use App\Models\Accounts;
use App\Models\User;
use App\Platforms\TPlatforms;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Http\Request;

class PlatformSiteController extends Controller
{
    use TPlatforms;

    public function login(Request $request, $platform)
    {
        return $this->pLogin([
            'platform' => $platform
        ]);
    }

    public function callback(Request $request, $platform)
    {
        return $this->pCallback([
            'platform' => $platform
        ]);
    }

    public function share(Request $request, $platform)
    {
        return $this->pShare([
            'platform' => $platform
        ]);
    }
}
