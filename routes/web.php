<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('/{platform}')->group(function () {
    Route::get('/', [\App\Http\Controllers\PlatformSiteController::class,'login']);
    Route::get('/callback', [\App\Http\Controllers\PlatformSiteController::class,'callback']);
    Route::get('/share', [\App\Http\Controllers\PlatformSiteController::class,'share']);

});
