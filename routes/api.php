<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function (){
    Route::prefix('settings')->group(function (){
        Route::any('/', [\App\Http\Controllers\SettingsController::class,'settings'])->name('settings');
        Route::any('optimize', [\App\Http\Controllers\SettingsController::class,'optimize'])->name('settings.optimize');
        Route::any('migrate', [\App\Http\Controllers\SettingsController::class,'migrate'])->name('settings.migrate');
    });

    Route::prefix('platform')->group(function (){
        Route::prefix('{platform}')->group(function (){
            Route::any('connect', [\App\Http\Controllers\PlatformController::class,'connect'])->name('platform.connect');
            Route::any('callback', [\App\Http\Controllers\PlatformController::class,'callback'])->name('platform.callback');
            Route::any('share', [\App\Http\Controllers\PlatformController::class,'share'])->name('platform.share');
            Route::any('publish', [\App\Http\Controllers\PlatformController::class,'publish'])->name('platform.publish');
        });
    });
});
