<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Store extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Store::create([
            'title' => 'Ron Digital'
        ]);
    }
}
