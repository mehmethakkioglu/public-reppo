<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UserToStore extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\UserToStore::create([
            'user_id' => 1,
            'store_id' => 1
        ]);
    }
}
