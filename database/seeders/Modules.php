<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Modules extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Modules::create([
            'title' => 'Kullanıcı',
            'name' => 'User',
            'description' => 'Kullanıcı yönetim modülü.'
        ]);

        \App\Models\Modules::create([
            'title' => 'Mağaza',
            'name' => 'Store',
            'description' => 'Mağaza yönetim modülü.'
        ]);

        \App\Models\Modules::create([
            'title' => 'Yetki',
            'name' => 'Permission',
            'description' => 'Yetki yönetim modülü.'
        ]);

        \App\Models\Modules::create([
            'title' => 'Paket',
            'name' => 'Package',
            'description' => 'Paket yönetim modülü.'
        ]);

        \App\Models\Modules::create([
            'title' => 'Platform',
            'name' => 'Platform',
            'description' => 'Platform yönetim modülü.'
        ]);

        \App\Models\Modules::create([
            'title' => 'Statü Grubu',
            'name' => 'StatusGroup',
            'description' => 'Statü Grubu yönetim modülü.'
        ]);

        \App\Models\Modules::create([
            'title' => 'Statü',
            'name' => 'Status',
            'description' => 'Statü yönetim modülü.'
        ]);

        \App\Models\Modules::create([
            'title' => 'Paylaşım',
            'name' => 'Shared',
            'description' => 'Paylaşım yönetim modülü.'
        ]);
    }
}
