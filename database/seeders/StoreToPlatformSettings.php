<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StoreToPlatformSettings extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\StoreToPlatformSettings::create([
            'store_id' => 1,
            'platform_id' => 1,
        ]);

        \App\Models\StoreToPlatformSettings::create([
            'store_id' => 1,
            'platform_id' => 2,
        ]);

        \App\Models\StoreToPlatformSettings::create([
            'store_id' => 1,
            'platform_id' => 3,
        ]);
    }
}
