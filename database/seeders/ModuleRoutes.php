<?php

namespace Database\Seeders;

use App\Models\ModuleRoute;
use Illuminate\Database\Seeder;

class ModuleRoutes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Kullanıcı Modülü
        ModuleRoute::create([
            'module_id' => 1,
            'route_name' => 'user.create',
            'title' => 'Kullanıcı Oluştur',
            'type' => 'create'
        ]);
        ModuleRoute::create([
            'module_id' => 1,
            'route_name' => 'user.read',
            'title' => 'Kullanıcı Listesi',
            'type' => 'read'
        ]);
        ModuleRoute::create([
            'module_id' => 1,
            'route_name' => 'user.update',
            'title' => 'Kullanıcı Düzenle',
            'type' => 'update'
        ]);
        ModuleRoute::create([
            'module_id' => 1,
            'route_name' => 'user.delete',
            'title' => 'Kullanıcı Sil',
            'type' => 'delete'
        ]);
        ModuleRoute::create([
            'module_id' => 1,
            'route_name' => 'user.view',
            'title' => 'Kullanıcı İncele',
            'type' => 'read'
        ]);

        // Mağaza Modülü
        ModuleRoute::create([
            'module_id' => 2,
            'route_name' => 'store.create',
            'title' => 'Mağaza Oluştur',
            'type' => 'create'
        ]);
        ModuleRoute::create([
            'module_id' => 2,
            'route_name' => 'store.read',
            'title' => 'Mağaza Listesi',
            'type' => 'read'
        ]);
        ModuleRoute::create([
            'module_id' => 2,
            'route_name' => 'store.update',
            'title' => 'Mağaza Düzenle',
            'type' => 'update'
        ]);
        ModuleRoute::create([
            'module_id' => 2,
            'route_name' => 'store.delete',
            'title' => 'Mağaza Sil',
            'type' => 'delete'
        ]);
        ModuleRoute::create([
            'module_id' => 2,
            'route_name' => 'store.view',
            'title' => 'Mağaza İncele',
            'type' => 'read'
        ]);

        // Yetki Modülü
        ModuleRoute::create([
            'module_id' => 3,
            'route_name' => 'permission.create',
            'title' => 'Yetki Oluştur',
            'type' => 'create'
        ]);
        ModuleRoute::create([
            'module_id' => 3,
            'route_name' => 'permission.read',
            'title' => 'Yetki Listesi',
            'type' => 'read'
        ]);
        ModuleRoute::create([
            'module_id' => 3,
            'route_name' => 'permission.update',
            'title' => 'Yetki Düzenle',
            'type' => 'update'
        ]);
        ModuleRoute::create([
            'module_id' => 3,
            'route_name' => 'permission.delete',
            'title' => 'Yetki Sil',
            'type' => 'delete'
        ]);
        ModuleRoute::create([
            'module_id' => 3,
            'route_name' => 'permission.view',
            'title' => 'Yetki İncele',
            'type' => 'read'
        ]);

        // Paket Modülü
        ModuleRoute::create([
            'module_id' => 4,
            'route_name' => 'package.create',
            'title' => 'Paket Oluştur',
            'type' => 'create'
        ]);
        ModuleRoute::create([
            'module_id' => 4,
            'route_name' => 'package.read',
            'title' => 'Paket Listesi',
            'type' => 'read'
        ]);
        ModuleRoute::create([
            'module_id' => 4,
            'route_name' => 'package.update',
            'title' => 'Paket Düzenle',
            'type' => 'update'
        ]);
        ModuleRoute::create([
            'module_id' => 4,
            'route_name' => 'package.delete',
            'title' => 'Paket Sil',
            'type' => 'delete'
        ]);
        ModuleRoute::create([
            'module_id' => 4,
            'route_name' => 'package.view',
            'title' => 'Paket İncele',
            'type' => 'read'
        ]);

        // Platform Modülü
        ModuleRoute::create([
            'module_id' => 5,
            'route_name' => 'platform.create',
            'title' => 'Platform Oluştur',
            'type' => 'create'
        ]);
        ModuleRoute::create([
            'module_id' => 5,
            'route_name' => 'platform.read',
            'title' => 'Platform Listesi',
            'type' => 'read'
        ]);
        ModuleRoute::create([
            'module_id' => 5,
            'route_name' => 'platform.update',
            'title' => 'Platform Düzenle',
            'type' => 'update'
        ]);
        ModuleRoute::create([
            'module_id' => 5,
            'route_name' => 'platform.delete',
            'title' => 'Platform Sil',
            'type' => 'delete'
        ]);
        ModuleRoute::create([
            'module_id' => 5,
            'route_name' => 'platform.view',
            'title' => 'Platform İncele',
            'type' => 'read'
        ]);

        // Statü Grubu Modülü
        ModuleRoute::create([
            'module_id' => 6,
            'route_name' => 'platform.create',
            'title' => 'Platform Oluştur',
            'type' => 'create'
        ]);
        ModuleRoute::create([
            'module_id' => 6,
            'route_name' => 'platform.read',
            'title' => 'Platform Listesi',
            'type' => 'read'
        ]);
        ModuleRoute::create([
            'module_id' => 6,
            'route_name' => 'platform.update',
            'title' => 'Platform Düzenle',
            'type' => 'update'
        ]);
        ModuleRoute::create([
            'module_id' => 6,
            'route_name' => 'platform.delete',
            'title' => 'Platform Sil',
            'type' => 'delete'
        ]);
        ModuleRoute::create([
            'module_id' => 6,
            'route_name' => 'platform.view',
            'title' => 'Platform İncele',
            'type' => 'read'
        ]);

        // Statü Modülü
        ModuleRoute::create([
            'module_id' => 7,
            'route_name' => 'status.create',
            'title' => 'Durum Oluştur',
            'type' => 'create'
        ]);
        ModuleRoute::create([
            'module_id' => 7,
            'route_name' => 'status.read',
            'title' => 'Durum Listesi',
            'type' => 'read'
        ]);
        ModuleRoute::create([
            'module_id' => 7,
            'route_name' => 'status.update',
            'title' => 'Durum Düzenle',
            'type' => 'update'
        ]);
        ModuleRoute::create([
            'module_id' => 7,
            'route_name' => 'status.delete',
            'title' => 'Durum Sil',
            'type' => 'delete'
        ]);
        ModuleRoute::create([
            'module_id' => 7,
            'route_name' => 'status.view',
            'title' => 'Durum İncele',
            'type' => 'read'
        ]);

        // Paylaşım Modülü
        ModuleRoute::create([
            'module_id' => 8,
            'route_name' => 'shared.create',
            'title' => 'Paylaşım Oluştur',
            'type' => 'create'
        ]);
        ModuleRoute::create([
            'module_id' => 8,
            'route_name' => 'shared.read',
            'title' => 'Paylaşım Listesi',
            'type' => 'read'
        ]);
        ModuleRoute::create([
            'module_id' => 8,
            'route_name' => 'shared.update',
            'title' => 'Paylaşım Düzenle',
            'type' => 'update'
        ]);
        ModuleRoute::create([
            'module_id' => 8,
            'route_name' => 'shared.delete',
            'title' => 'Paylaşım Sil',
            'type' => 'delete'
        ]);
        ModuleRoute::create([
            'module_id' => 8,
            'route_name' => 'shared.view',
            'title' => 'Paylaşım İncele',
            'type' => 'read'
        ]);
    }
}
