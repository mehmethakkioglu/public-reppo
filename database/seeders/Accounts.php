<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Accounts extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Accounts::create([
            'title' => 'Facebook Hesabım',
            'store_id' => 1,
            'platform_id' => 1,
            'app_id' => '110711744760669',
            'options' => "{'app_id':'326082742671209','app_secret':'7e326f49e4b1362ec9e71c5f329c68b4','default_graph_version':'v12.0','token':'EAAEokg4LV2kBALDjgauRPQuNfYsp1MV4DRqofLTGDuWaHco3sDn2w7onv6EtFrjbT7Y7uUoZANkvOVqL8F86eQfG12dxkvEoVzmxvRYcXM8RR1Glj2rvbjHFabSVDDuXUEWy1L2SedmqbCncJVVul1n0dAvKHOAfTIKZBRuVAyrH1CNZBWL4XnKiu5SsB77ZC1jm24qdmQZDZD'}"
        ]);

        \App\Models\Accounts::create([
            'title' => 'Instagram Hesabım',
            'store_id' => 1,
            'platform_id' => 2,
            'app_id' => '17841405896007275',
            'options' => "{'app_id':'326082742671209','app_secret':'7e326f49e4b1362ec9e71c5f329c68b4','default_graph_version':'v12.0','token':'EAAEokg4LV2kBALDjgauRPQuNfYsp1MV4DRqofLTGDuWaHco3sDn2w7onv6EtFrjbT7Y7uUoZANkvOVqL8F86eQfG12dxkvEoVzmxvRYcXM8RR1Glj2rvbjHFabSVDDuXUEWy1L2SedmqbCncJVVul1n0dAvKHOAfTIKZBRuVAyrH1CNZBWL4XnKiu5SsB77ZC1jm24qdmQZDZD'}"
        ]);

        \App\Models\Accounts::create([
            'title' => 'Twitter Hesabım',
            'store_id' => 1,
            'platform_id' => 3,
            'app_id' => null
        ]);

        \App\Models\Accounts::create([
            'title' => 'Google Hesabım',
            'store_id' => 1,
            'platform_id' => 4,
            'app_id' => null
        ]);
    }
}
