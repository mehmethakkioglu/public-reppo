<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Status extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Status::create([
            'status_group_id' => 1,
            'title' => 'Onay Bekliyor'
        ]);

        \App\Models\Status::create([
            'status_group_id' => 1,
            'title' => 'Onaylandı'
        ]);

        \App\Models\Status::create([
            'status_group_id' => 1,
            'title' => 'Onaylanmadı'
        ]);
    }
}
