<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AccountsToUser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\AccountsToUser::create([
            'user_id' => 1,
            'store_id' => 1,
            'platform_id' => 1,
            'account_id' => 1
        ]);

        \App\Models\AccountsToUser::create([
            'user_id' => 1,
            'store_id' => 1,
            'platform_id' => 2,
            'account_id' => 2
        ]);

        \App\Models\AccountsToUser::create([
            'user_id' => 1,
            'store_id' => 1,
            'platform_id' => 3,
            'account_id' => 3
        ]);

        \App\Models\AccountsToUser::create([
            'user_id' => 1,
            'store_id' => 1,
            'platform_id' => 4,
            'account_id' => 4
        ]);
    }
}
