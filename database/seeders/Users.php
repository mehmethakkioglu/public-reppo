<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class Users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Mehmet HAKKIOĞLU',
            'email' => 'mehmet@ron.digital',
            'password' => passwordGenerate([
                'email' => 'mehmet@ron.digital',
                'password' => '123'
            ]),
        ]);
    }
}
