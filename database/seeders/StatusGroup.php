<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StatusGroup extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\StatusGroup::create([
            'title' => 'Genel',
            'description' => 'Genel statüler bu kategori altında toplanmaktadır.'
        ]);
    }
}
