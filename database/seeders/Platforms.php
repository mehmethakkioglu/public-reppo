<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Platforms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Platforms::create([
            'title' => 'Facebook'
        ]);

        \App\Models\Platforms::create([
            'title' => 'Instagram'
        ]);

        \App\Models\Platforms::create([
            'title' => 'Twitter'
        ]);

        \App\Models\Platforms::create([
            'title' => 'Google'
        ]);
    }
}
